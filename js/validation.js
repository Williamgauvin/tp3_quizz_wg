const quiz = {}
quiz.questionCourante = 0
quiz.reponses = []
quiz.bonneReponse = 0

const donneesEntrantes = `
[ {
    "question":"Question 1.<br>Who let the dogs out?",
    "reponses": [ "Who?", "Ooo!", "Où", "Wu" ],
    "bonneReponse":0
    },{
    "question":"Question 2.<br>Who do you think you are?",
    "reponses": [ "Jackity", "Frakity", "Beaukity", "Packity" ],
    "bonneReponse":3
    },{
        "question":"Question 3.<br>What kind of question is this",
        "reponses": [ "Joel", "Repo", "Glouti", "Guwop" ],
        "bonneReponse":1
    },{
    "question":"Question 4.<br>1+1=?",
    "reponses": [ "1", "1.2", "2", "-2" ],
    "bonneReponse":3
    },{
        "question":"Question 5.<br>What's Updog?",
        "reponses": [ "hm", "aaa", "aaaaaaa", "aaaaa" ],
        "bonneReponse":3
        }
        
    
]
`
const questions = JSON.parse(donneesEntrantes)

const resultsContainer = document.getElementById('results')







function getTodayString() {
    const today = new Date();
    const todayString1 = "2021-04-28";
    const fullYear = String(today.getFullYear());
    const month = String(today.getMonth()+1).padStart(2,"0");
    const day = String(today.getDate());
    // todayString2 = "2021-04"
    // on utilise padStart(longueur, caractère)
    const todayString = fullYear + "-" + month + "-" + day;
    return todayString;
}

// https://stackoverflow.com/a/17312792/2779871
// value = "2021-04-27"
function beforeToday(value) {
    var curDate = getTodayString();
    var inputdate = value;
    const result = inputdate < curDate;
    return result;
    // "2021-04-27" < "2021-04-28" → true
}



function AjouterValidation() {

    const jquery = $;
    const today = getTodayString();
    const formulaire = jquery("form[name='creation-quiz']");
    formulaire.validate(
        {
            rules: {
                prenom: 'required',
                nom: 'required',
                date: {
                    required: true,
                    beforeToday: true
                },
                statut: 'required',
            },
            message: {
                prenom: 'Prenom requis',
                nom: 'Nom requis',
                date: 'Date requise',
                statut: 'Statut requis'
            },
            submitHandler: function (form) {
                quiz.prenom = $('#prenom').val()
                quiz.nom = $('#nom').val()
                quiz.date = $('#date').val()
                quiz.statut = $('#statut').val()
                creerQuiz()
                $('#formulaire').remove()
                
            }


        }
    )
}

function creerQuiz() {
    AfficherQuestionQuiz()
    
    const bouton = $('<button>Soumettre</button>')    
    bouton.attr( { id: "soumettre-reponse", class: "marginTop-m cta", type: "submit" } )
    bouton.on('click', function(){
        quiz.questionCourante += 1
        const reponse = $('input[name=question]:checked').val()
        quiz.reponses.push(reponse)
        $('#question').remove()
        if(quiz.questionCourante >= questions.length){   
            afficherRapport() 
        }else {

            AfficherQuestionQuiz()

        }
    })
    
    $('#conteneur').append(bouton)
}

function AfficherQuestionQuiz() {

    const questionCourante = questions[quiz.questionCourante]
    const label = $('<h2>' + questionCourante.question + '</h2>')
    let bonnesReponses = 0;

    const question = $('<div id="question"></div>')
    $("#conteneur").append(question),
    // if(quiz.reponses === questionCourante.bonneReponse){
    //     bonnesReponses++;}

            
    question.append(label)
    for (let i = 0; i < questionCourante.reponses.length; i++){
        const reponse = $('<input></input>')
        reponse.attr({type: "radio", class: "brand", name: "question"})
        reponse.attr('value', questionCourante.reponses[i])
        const label = $('<label>'+ questionCourante.reponses[i] + '</lable>')
        question.append(reponse)
        question.append(label)
    }

    

    // }


}

function afficherRapport(){


    const conteneur = $('#conteneur');
    const results = $('<div id="results"> Résultats:');
    for (let i = 0; i < quiz.reponses.length; i++){
        const paragraph = $('<p></p>')
        paragraph.append(quiz.reponses[i]);
        results.append(paragraph);
    }
    conteneur.append(results);
    $('#soumettre-reponse').remove()




}





// function validationReponse(){


//     const questionCourante = questions[quiz.questionCourante]
//     let bonnesReponses = 0;





// }

$(function () {
    AjouterValidation()
})

// #1: nom de la méthode
$.validator.addMethod("beforeToday", beforeToday, "Date Invalide");